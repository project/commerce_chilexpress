<?php

/**
 * @file
 * The format for Chile adresses to use with Chilexpress.
 */

$plugin = array(
  'title'           => t('Address form (specific for Chile - Chilexpress).'),
  'format callback' => 'chilexpress_format_address',
  'type'            => 'address',
  'weight'          => 0,
);


/**
 * The format callback.
 */
function chilexpress_format_address(&$format, $address, $context = array()) {
  if ($address['country'] == 'CL') {
    $use_provinces = variable_get('addressfield_cl_provinces', FALSE);

    unset($format['locality_block']['locality']);

    $options = array(
      '' => t('--'),
    );

    $options += chilexpress_get_regions();
    $format['locality_block']['administrative_area'] = array(
      '#title'              => t('Region'),
      '#options'            => $options,
      '#required'           => TRUE,
      '#render_option_value'=> TRUE,
      '#attributes'         => array('class' => array('locality')),
      '#weight'             => 1,
    );

    $communes_options = array(
      '' => t('--'),
    );

    $communes_options += chilexpress_get_communes(CHILEXPRESS_COVER_TYPE_DELIVERY, $address['administrative_area']);

    $format['locality_block']['dependent_locality'] = array(
      '#title'              => t('Commune'),
      '#size'               => 20,
      '#render_option_value'=> TRUE,
      '#tag'                => 'div',
      '#required'           => TRUE,
      '#options'            => $communes_options,
      '#attributes'         => array('class' => array('dependent', 'commune')),
      '#weight'             => 3,
    );
  }

  // Format render
  if ($context['mode'] == 'render') {
    $format['locality_block']['dependent_locality']['#weight'] = 1;
    $format['locality_block']['administrative_area']['#weight'] = 3;
  }

  if ($context['mode'] == 'form' && $address['country'] == 'CL') {
    $ajax_options = array(
      'callback' => 'addressfield_standard_widget_refresh',
      'wrapper' => $format['#wrapper_id'],
    );
    $format['locality_block']['administrative_area']['#ajax'] = $ajax_options;
  }
}

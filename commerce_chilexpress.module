<?php


/**
 * Implements hook_menu().
 */
function commerce_chilexpress_menu() {
  $items = array();
  $items['admin/commerce/config/shipping/methods/chilexpress/edit'] = array(
    'title' => 'Edit',
    'description' => 'Adjust Chilexpress shipping settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_chilexpress_settings_form'),
    'access arguments' => array('administer shipping'),
    'file' => 'commerce_chilexpress.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_INLINE,
    'weight' => 0,
  );

  return $items;
}

/**
 * Implements hook_commerce_shipping_method_info().
 */
function commerce_chilexpress_commerce_shipping_method_info() {
  $shipping_methods = array();

  $shipping_methods['chilexpress'] = array(
    'title' => t('Chilexpress'),
    'description' => t('Quote rates from Chilexpress'),
  );

  return $shipping_methods;
}


/**
 * Implements hook_ctools_plugin_directory().
 */
function commerce_chilexpress_ctools_plugin_directory($module, $plugin) {
  if ($module == 'addressfield') {
    return 'plugins/' . $plugin;
  }
}


/**
 * Implements hook_commerce_shipping_service_info().
 */
function commerce_chilexpress_commerce_shipping_service_info() {
  $shipping_services = array();

  $available_services = commerce_chilexpress_shipping_service_types();

  foreach ($available_services as $id => $service_name) {
    $shipping_services['chilexpress_type_' . $id] = array(
      'title' => $service_name,
      'description' => variable_get('commerce_chilexpress_service_type_' . $id, 'Chilexpress ' . $service_name),
      'display_title' => $service_name,
      'shipping_method' => 'chilexpress',
      'price_component' => 'shipping',
      'callbacks' => array(
        'rate' => 'commerce_chilexpress_service_rate',
      ),
    );
  }

  return $shipping_services;
}

/**
 * Implements hook_addressfield_administrative_areas_alter().
 */
function commerce_chilexpress_addressfield_administrative_areas_alter(&$administrative_areas) {
  $administrative_areas['CL'] = chilexpress_get_regions();
}


function commerce_chilexpress_calculate_pricing($order) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  $field_name = commerce_physical_order_shipping_field_name($order);
  // Prepare the shipping address for use in the request.
  if (!empty($order_wrapper->{$field_name}->commerce_customer_address)) {
    $shipping_address = $order_wrapper->{$field_name}->commerce_customer_address->value();
  }
  else {

    return FALSE;
  }

  if ($shipping_address['country'] != 'CL') {
    return FALSE;
  }

  $destination = $shipping_address['dependent_locality'];
  $origin = variable_get('commerce_chilexpress_commune_origin', 'STGO');
  $weight = commerce_physical_order_weight($order, 'kg');
  // @TODO: Cambiar volume por dimensions
  $volume = commerce_physical_order_volume($order, 'cm');
  $dimension = round(pow($volume['volume'], 1/3), 0);

  $values = array(
    'origin' => $origin,
    'destination' => $destination,
    'long' => $dimension,
    'width' => $dimension,
    'high' => $dimension,
    'weight' => $weight['weight'],
  );

  return chilexpress_calculate_pricing($values);
}


/**
 *
 */
function commerce_chilexpress_service_rate($shipping_service, $order) {
  $rates = commerce_shipping_rates_cache_get('chilexpress', $order, 0);
  if (empty($rates)) {
    $results = commerce_chilexpress_calculate_pricing($order);
    if ($results) {
      foreach ($results as $result) {
        $service_name = 'chilexpress_type_' . $result->CodServicio;
        if (!variable_get('commerce_chilexpress_service_type_' . $result->CodServicio . '_active', TRUE)) {
          continue;
        }
        $amount = $result->ValorServicio;
        $rates[$service_name] = array(
          'amount' => commerce_currency_decimal_to_amount($amount, 'CLP'),
          'currency_code' => 'CLP',
          'data' => array(),
        );
      }
      if (!empty($rates)) {
        commerce_shipping_rates_cache_set('chilexpress', $order, $rates);
      }
      else {
        commerce_shipping_rates_cache_set('chilexpress', $order, array());
      }
    }
  }

  // Return the rate for the requested service or FALSE if not found.
  return isset($rates[$shipping_service['name']]) ? $rates[$shipping_service['name']] : FALSE;
}

/**
 * Chilexpress services types available.
 */
function commerce_chilexpress_shipping_service_types() {
  return array(
    1 => t('Very Fast'),
    2 => t('Overnight'),
    3 => t('Next Business Day'),
    4 => t('Following business day'),
    5 => t('Third business day'),
    6 => t('International'),
    7 => t('Eighth day'),
    8 => t('AM/PM'),
    10 => t('2 Hours'),
    11 => t('Saturday Delivery'),
    12 => t('Overnight Priority'),
  );
}

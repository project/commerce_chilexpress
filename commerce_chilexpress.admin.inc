<?php

/**
 * @file
 */


/**
 * Admin form to define the commune origin of the shipping.
 */
function commerce_chilexpress_settings_form($form, &$form_state) {
  $communes = chilexpress_get_communes(CHILEXPRESS_COVER_TYPE_ADMISSION);
  ksort($communes);

  $form['commerce_chilexpress_commune_origin'] = array(
    '#type' => 'select',
    '#title' => t('Origin'),
    '#options' => $communes,
    '#default_value' => variable_get('commerce_chilexpress_commune_origin', 'STGO'),
    '#required' => TRUE,
    '#description' => t('Define commune origin.'),
  );


  $form['available_services'] = array(
    '#type' => 'fieldset',
    '#title' => t('Available Services'),
    '#description' => t('Define if this services will be active and the description of the services when appears to the user.'),
  );

  $available_services = commerce_chilexpress_shipping_service_types();
  foreach ($available_services as $id => $service_name) {
    $form['available_services'][$id] = array(
      '#type' => 'fieldset',
      '#title' => t('Service %service_name', array('%service_name' => $service_name)),
    );
    $form['available_services'][$id]['commerce_chilexpress_service_type_' . $id . '_active'] = array(
      '#type' => 'checkbox',
      '#title' => t('Active'),
      '#default_value' => variable_get('commerce_chilexpress_service_type_' . $id . '_active', TRUE),
    );
    $form['available_services'][$id]['commerce_chilexpress_service_type_' . $id] = array(
      '#type' => 'textfield',
      '#title' => t('Description'),
      '#default_value' => variable_get('commerce_chilexpress_service_type_' . $id, 'Chilexpress ' . $service_name),
      '#required' => TRUE,
    );
  }

  return system_settings_form($form);
}
